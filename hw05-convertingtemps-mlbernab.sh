#ask option for conversion F, C, and K 
#user input of temperature depending on temp
#if F, C, K entered convert to all depending on formula 
#conversion Fahrenheit to Celsius: C = (F - 32) * 5/9
#conversion Celsius to Fahrenheit: F = (C * 9/5) + 32
#conversion from Celsius to Kelvin: K = C + 273.15
#conversion from Kelvin to Celsius: C = K - 273.15
#conversion from Fahrenheit to Kelvin: K = (F - 32) * 5/9 + 273.15
#conversion from Kelvin to Fahrenheit: F = (K - 273.15) * 9/5 + 32
#!/usr/bin/bash
echo "Convert temperatures: (F)ahrenheit, (C)elsius or (K)elvin "
read temp
if [ $temp = F ]
then
	echo "Enter Temperature: "
	read f
	c=$(echo "scale=2; ($f - 32)*5/9" | bc)
	k=$(echo "scale=2; (($f - 32)*5/9)+273.15" | bc)
	echo "Fahrenheit = $f "
	echo "Celsius = $c "
	echo "Kelvin = $k "
elif [ $temp = C ]
then 
	echo "Enter Temperature: "
	read c
	f=$(echo "scale=2; ($c * 9/5)+32" | bc)
	k=$(echo "scale=2; ($c + 273.15)" | bc)
        echo "Fahrenheit = $f "
        echo "Celsius = $c "
        echo "Kelvin = $k "
elif [ $temp = K ]
then 
	echo "Enter Temperature: "
	read k
	f=$(echo "scale=2; ($k - 273.15)*(9/5)+32" | bc)
	c=$(echo "scale=2; ($k - 273.15)" | bc)
        echo "Fahrenheit = $f "
        echo "Celsius = $c "
        echo "Kelvin = $k "
fi
